Database design, control, and versioning to enforce 100% transparency of all database modifications


# What does this stp-database project do? 
    
    It stores and SQL database schemas and docs and versions them using git

## Why?
**It secures database designs and provides complete access to them**
- **secures change history creation with RSA public key authentication**
- records database design modifications, forks, concepts, trials, etc.
- centralizes change control, and **opens global data design, collab and contrib**
- **expands access to database knowledge management** of all stp web projects
- centralizes security administration and secures the database design documents

**Exposes all** of the following information regarding stp database designs
- timelines
- milestones
- issues
- versions
- activity
- contributions
- branches of current and past developement
- complete change history logging and archival


----
In short, this project **exposes** absolutely everything about stp database design